FROM python:3

WORKDIR /pyrp

COPY . .

RUN pip install poetry
RUN python3 -m poetry install

CMD [ "python3", "-m", "poetry", "run", "python3", "pyrp/main.py" ]