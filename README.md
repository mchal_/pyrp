# PyRP
Python-based Roleplay bot that supports Homestuck-like text quirks

## 💡 Features

- Dynamically reloads character list
- Simple, yet powerful use of Python's inbuilt regex module
- **NEW**: Uses my amazing new bot library, [Peachtree](https://gitlab.com/mchal_/peachtree)!


## 💾 Installation

1. Clone PyRP to a directory of your choice
    ```bash
    $ git clone https://gitlab.com/mchal_/pyrp
    ```

2. Edit the `pyrp/characters.py` file to your liking
3. Run using poetry
    ```bash
    $ poetry run python3 pyrp/main.py
    ```

## 📜 License

[GPLv3-only](https://choosealicense.com/licenses/gpl-3.0/)
