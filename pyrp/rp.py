import importlib
import json
import os
import re
from typing import Dict

from peachtree.main import commands, logger, respond

import characters as characters_module
from classes import get_md5

characters_md5 = get_md5("pyrp/characters.py")


def get_updated_characters():
    global characters_md5
    current_characters_md5 = get_md5("pyrp/characters.py")
    if current_characters_md5 != characters_md5:
        logger.info("Change in `characters.py` detected. Reloading module.")
        importlib.reload(characters_module)
        logger.debug("Reloaded `characters.py`")
        characters_md5 = current_characters_md5

    return characters_module.chars


async def respond_as_character(ctx, text, character):
    if not text.startswith("*"):
        for i in character.quirk:
            text = re.sub(i[0], i[1], text)

    logger.info(f'Sending "{text}" as "{character.name}"')
    logger.debug(f"User: {ctx.author}. Server: {ctx.guild.id}")
    await ctx.response.send_message(
        embed=character.embed(
            text,
        )
    )


class RolePlay(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.slash_command(
        name="as",
        description="Send a one-off message using a specific character.",
    )
    async def as_(self, ctx, alias: str, text: str):
        all_characters = get_updated_characters()

        for character in all_characters:
            if character.alias == alias:
                await respond_as_character(ctx, text, character)
                break
        else:
            await self.handle_invalid_character(ctx, alias, all_characters)

    @commands.slash_command(
        name="set", description="Set a character as your default character"
    )
    async def set(self, ctx, alias: str):
        all_characters = get_updated_characters()

        if not os.path.isfile("data.json"):
            logger.debug("No data.json file found. Creating")
            with open("data.json", "x") as f:
                json.dump({}, f, indent=4)
                logger.debug("Created data.json")

        with open("data.json", "r") as f:
            logger.debug("Loading data.json")
            data: Dict = json.load(f)
            logger.debug("Loaded data.json")

        for character in all_characters:
            if character.alias == alias:
                with open("data.json", "wt") as f:
                    logger.debug(f"Setting {alias} as default for {ctx.author.id}")
                    data[str(ctx.author.id)] = alias
                    json.dump(data, f, indent=4)
                    logger.debug(f"Default set for {ctx.author.id}")

                await respond(
                    ctx,
                    self.bot,
                    f"**{character.name}** has been set as your default character",
                    ephemeral=True,
                )
                break
        else:
            await self.handle_invalid_character(ctx, alias, all_characters)

    @commands.slash_command(
        name="say", description="Send a message as your default character (see /set)"
    )
    async def say(self, ctx, text: str):
        all_characters = get_updated_characters()

        with open("data.json", "r") as f:
            data: Dict = json.load(f)
            default_char = data[str(ctx.author.id)]

        for character in all_characters:
            if character.alias == default_char:
                await respond_as_character(ctx, text, character)

    async def handle_invalid_character(self, ctx, alias, characters_):
        logger.error(f'Character not found "{alias}"')
        logger.debug(f"User: {ctx.author}, Server: {ctx.guild_id}")

        valid_characters_string = "\n".join(
            map(lambda x: ("`" + x.alias + "` - " + x.name), characters_)
        )

        await respond(
            interaction=ctx,
            bot=self.bot,
            message=f"Character not found `{alias}`.\n\n"
            + f"Valid characters are:\n\n"
            + valid_characters_string,
            error=True,
            ephemeral=True,
        )
