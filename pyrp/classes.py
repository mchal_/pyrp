import hashlib
from dataclasses import dataclass
from enum import Enum

from peachtree.main import disnake


class Color(Enum):
    RUST = (0xA2, 0x00, 0x00)
    BRONZE = (0xA1, 0x52, 0x03)
    GOLD = (0xA1, 0xA1, 0x00)
    LIME = (0x67, 0x89, 0x00)
    OLIVE = (0x33, 0x66, 0x01)
    JADE = (0x08, 0x84, 0x46)
    TEAL = (0x00, 0x82, 0x82)
    COBALT = (0x00, 0x41, 0x83)
    INDIGO = (0x01, 0x21, 0xCC)
    PURPLE = (0x45, 0x0A, 0x80)
    VIOLET = (0x6A, 0x01, 0x6A)
    FUCHSIA = (0x99, 0x01, 0x4E)
    UNKNOWN = (0x62, 0x62, 0x62)

    def rgb(self):
        return disnake.Colour.from_rgb(self.value[0], self.value[1], self.value[2])


@dataclass
class Character:
    name: str
    username: str
    image_url: str
    alias: str
    color: Color = Color.UNKNOWN
    quirk: list = list

    def embed(
        self,
        message: str,
    ):
        embed = disnake.Embed(
            title=self.username,
            description=message,
            colour=self.color.rgb(),
        )
        embed.set_thumbnail(url=self.image_url)
        embed.set_footer(text=self.name)

        return embed


def get_md5(string: str) -> str:
    with open(string, "rt") as f:
        file_hash = hashlib.md5()
        file_hash.update(f.read().encode())
    return file_hash.hexdigest()
