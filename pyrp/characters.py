from classes import Color, Character

chars = [
    Character(
        name="Todd Example",
        username="exemplaryCharacter",
        image_url="https://thumbs.dreamstime.com/z/regular-guy-thumbsup-3449771.jpg",
        alias="ec",
        color=Color.UNKNOWN,
        quirk=[],  # List of find & replace regex tuples, e.g. ("a|A", "@"), can also take python functions,
        # see https://docs.python.org/3/library/re.html
    ),
]
