import yaml
from peachtree.main import Bot

from pyrp.rp import RolePlay

with open("config.yml", "r") as f:
    token = yaml.load(f, Loader=yaml.Loader)["token"]

pyrp = Bot(name="PyRP", version="0.2.0", token=token, author="mchal_")

pyrp.cogs([RolePlay(pyrp)])
pyrp.start()
